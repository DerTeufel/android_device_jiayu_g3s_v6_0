#!/bin/sh

BASE=../../../vendor/jiayu/G3S_V6_0/proprietary
rm -rf $BASE/*

for FILE in `cat proprietary-files.txt`; do
    DIR=`dirname $FILE`
    if [ ! -d $BASE/$DIR ]; then
        mkdir -p $BASE/$DIR
    fi
    if [ ! -f $BASE/$FILE ]; then
    adb pull /system/$FILE $BASE/$FILE
    echo "pulled $FILE to $BASE/$FILE"
    sleep 1
    fi
done

# some extra stuff
adb pull /system/lib/hw/audio_policy.default.so $BASE/lib/hw/audio_policy.mt6589.so
adb pull /system/lib/libaudio.primary.default.so $BASE/lib/hw/audio.primary.mt6589.so

./setup-makefiles.sh

